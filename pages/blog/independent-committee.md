title=Adblock Plus parent eyeo announces members of independent Acceptable Ads Committee
description=The criteria for what makes an ad acceptable, or not, for users of AdBlock, Adblock Plus and Crystal is now in the hands of the Acceptable Ads Committee.
featured_img_url=/img/png/blog/acceptable-ads-committee-structure.png
featured_img_alt=Acceptable Ads committee structure diagram
author=Ben Williams
committee=true
published_date=2017-03-15
template=blog-entry
breadcrumb=Independent committee
parent=blog/index

eyeo, parent company of Adblock Plus, today officially announced the first members of the Acceptable Ads Committee (AAC), an independent group which will take over control of its [Acceptable Ads program](https://acceptableads.com/). The Acceptable Ads program began in 2011, when Adblock Plus started letting users block most ads, but permitting ads that met criteria for acceptability – so long as the user chose to participate. The AAC will take full authority over these criteria and the compromise between users who want to block intrusive ads and publishers who rely on advertising revenues.

Also, in an industry first, an ordinary user will be seated as one of the 11 committee members  to represent consumers’ demands of the online advertising industry.

The AAC will take over management of the [Acceptable Ads criteria](https://acceptableads.com/en/about/criteria) and the rules for whitelisting that [Adblock Plus](https://adblockplus.org/en/about), [AdBlock](https://getadblock.com/), [Adblock Browser](https://adblockbrowser.org/) and [Crystal](http://crystalapp.co/) all abide by to offer users the ability to block unwanted ads while allowing through better, more respectful ones for those users who choose to view ads. While eyeo is facilitating the recruitment of the members constituting the initial AAC, the various member groups represented on the committee itself will seat future committees.

The AAC can have a maximum of 11 committee seats, representing nine member groups. At this time, there are eight committee seats filled: two of the (three) digital rights seats and the user seat have yet to be finalized. The user seat will be filled at the end of an ongoing recruitment process over various social media channels, and will represent the very first time that an actual user will have an equal part in a decision-making process with advertising ecosystem professionals.

The full-time committee members who took a seat are listed here:

[https://acceptableads.com/committee/members](https://acceptableads.com/committee/members).

### For-profit coalition {: #for-profit }

Advertisers
:    Breanna Fowler (Dell, seat)

Ad-tech
:    Ari Levenfeld (Rocket Fuel Inc., seat); Rakuten Marketing; Native Ads, Inc.; Sharethrough; The Media Trust; BuySellAds.com Inc.; Criteo; ZEDO; Bidio, Inc.; Instinctive, Inc.; Adtoma AB

Advertising agencies
:     Gabriel Cheng (M&C Saatchi Mobile, seat); The Tombras Group; Saatchi & Saatchi; TLGG; Schaaf-PartnerCentric; Look Listen

Publishers and content creators
:    Kenton Jacobsen (Condé Nast, seat); Dennis Publishing; Local Media Consortium; TED Talks; Leaf Group

### Expert coalition {: #experts }

Creative agent
:    [Open](https://acceptableads.com/en/committee/apply)

Researcher
:    Fran Howarth (Bloor Research International Ltd.)

User agent
:    Kenny Ye (叶智聪) (UC Web -maker of UC Browser, a subsidiary of Alibaba)

### User advocate coalition {: #user-advocates }

##### Digital rights organizations (x3): {: #digital-rights-organizations }

- Holmes Wilson (Fight for the Future, seat)
- [Open](https://acceptableads.com/en/committee/apply)
- [Open](https://acceptableads.com/en/committee/apply)

##### Users {: #users }

Recruiting starts March 15.

The application process for joining the AAC is still ongoing and continuous. Interested parties should [please apply here](https://acceptableads.com/en/committee/apply).

### Acceptable Ads Committee {: #acceptable-ads-committee }

The Acceptable Ads Committee (AAC) will be a formal committee of a nonprofit organization, Acceptable Ads Committee, Inc. The committee will meet at least two times a year and will be comprised of up to 11 official members. While these 11 committee members will sit officially at the meetings, each member group may contain up to 50 members; AAC members therefore are representatives of their respective member groups and are in charge of facilitating conversations within said groups regarding matters considered at AAC meetings.

*This blog post has been corrected so that members listed here are aligned with their respective listings on our accompanying [members page](https://acceptableads.com/committee/members/).*
