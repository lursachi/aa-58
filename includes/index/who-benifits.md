
## {{who-benefits-heading[Who benefits from Acceptable Ads heading] Who benefits from Acceptable Ads? }}

<div class="group group-5 cards">
  <? include index/who-benefits/publishers ?>
  <? include index/who-benefits/ad-networks ?>
  <? include index/who-benefits/advertisers ?>
  <? include index/who-benefits/ad-tech-suppliers ?>
  <? include index/who-benefits/users ?>
</div>
