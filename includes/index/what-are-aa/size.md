
<div class="item" markdown="1">

### {{size-heading[What are Acceptable Ads > 03. Size heading] Size }}

{{size-body[What are Acceptable Ads > 04. Size body] There are strict size requirements for individual ad sizes, as well as the total space occupied by ads. }}
{: .item-summary }

[{{size-learn-more[What are Acceptable Ads > 03. Size learn more button] Learn more }}](about/criteria#size){: .btn-outline-primary }

</div>
