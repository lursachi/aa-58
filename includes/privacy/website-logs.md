#### {{website-logs-heading[Heading of a section] Website logs}}

{{website-logs-paragraph[Website logs paragraph] While using the Website, we automatically record website logs and thereby collect the following data for technical and for security reasons:}}

* {{website-logs-list[Website logs list item] IP address (stored separately)}}
* {{website-logs-list2[Website logs list item] Date and time of access}}
* {{website-logs-list3[Website logs list item] Browser name/version}}[^1]
* {{website-logs-list4[Website logs list item] URL of previously visited webpage}}[^2]
* {{website-logs-list5[Website logs list item] Amount of data sent}}

{{website-logs-paragraph2[Website logs paragraph] This data is stored purely for technical reasons and cannot be linked to any individual person. We do not combine the website log data with any other information about you. Such website logs are retained for a period of <fix>30</fix> days, after which only the aggregated usage statistics that cannot be connected to a single user remain. Everything else is deleted.}}

[^1]: {{specific-interpretation[Website logs footnote] For more information, please refer to}} [https://tools.ietf.org/html/rfc7231#section-5.5.3](https://tools.ietf.org/html/rfc7231#section-5.5.3)
[^2]: {{ specific-interpretation }} [https://tools.ietf.org/html/rfc7231#section-5.5.2](https://tools.ietf.org/html/rfc7231#section-5.5.2)
