
##### {{footer-col-1-header[Footer column 1 header] Find Acceptable Ads on}}

- [{{footer-abp-desktop[Text of the link in the footer] <fix>Adblock</fix> Plus for Desktop Browsers}}](http://adblockplus.org/)
- [{{footer-abb[Text of the link in the footer] <fix>Adblock Browser</fix> for <fix>iOS</fix> and <fix>Android</fix>}}](https://adblockbrowser.org/)
- [{{footer-abp-safari[Text of the link in the footer] <fix>Adblock Plus</fix> for <fix>Safari</fix> for <fix>iOS</fix>}}](https://itunes.apple.com/app/adblock-plus-abp/id1028871868)
- [{{footer-adblock[Text of the link in the footer] <fix>​AdBlock</fix> products}}](https://getadblock.com/)
- [{{footer-adblock-premium[Text of the link in the footer] <fix>​AdBlock Premium</fix> products}}](https://www.getadblockpremium.com/)
- [{{footer-ublock[Text of the link in the footer] <fix>​uBlock</fix> products}}](https://www.ublock.org/)
- [{{footer-cristal[Text of the link in the footer] <fix>Crystal</fix> products}}](http://crystalapp.co/)
