
### Foregrounds

This block is muted
{: .muted }

This *word*{: .muted } is muted

This block is accent color
{: .accent }

This *word*{: .accent } is accent color

This block is error color
{: .error }

This *word*{: .error } is error color
