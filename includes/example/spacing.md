
This has an extra large margin bottom
{: .m-b-xl .bg-info }

This has a large margin bottom
{: .m-b-lg .bg-info }

This has a medium margin bottom
{: .m-b-md .bg-info }

This has a small margin bottom
{: .m-b-sm .bg-info }

This has an extra small margin bottom
{: .m-b-xs .bg-info }
