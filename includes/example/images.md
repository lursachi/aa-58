
### Images

![Alt text](/img/placeholders/placeholder-masthead-wide.png){: .block }

![Alt text](/img/placeholders/placeholder-square.png){ .sol .xl2 }

**Image floating <abbr title="Start Of Line">SOL</abbr>.**

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
{: .clearfix }

![Alt text](/img/placeholders/placeholder-square.png){: .eol .clear .xl2 }

**Image floating <abbr title="End Of Line">EOL</abbr>.**

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
{: .clearfix }
