
{{size-criteria[Paragraph in "General criteria -> Size"] Individual ad-size requirements depend on the placement of the ad:}}

* {{above-primary-content[Bullet point in "General criteria -> Size"] When placed above the primary content, the maximum height of an ad should be 200px.}}
* {{side-primary-content[Bullet point in "General criteria -> Size"] When placed on the side of the primary content, the maximum width of an ad should be 350px.}}
* {{below-primary-content[Bullet point in "General criteria -> Size"] When placed below the primary content, the maximum height of an ad should be 400px.}}

{{sufficient-space[Paragraph in "General criteria -> Size"] Ads must always leave sufficient space for the Primary Content on the common screen size of 1366x768 for desktop, 360x640 for mobile devices and 768x1024 for tablets.}} [^1] [^2] [^3]

{{total-ad-space[Paragraph in "General criteria" -> Size] All ads that are placed above the fold (the portion of the webpage visible in the browser window when the page first loads under the common screen size) must not occupy in total more than 15 percent of the visible portion of the web page. If placed below the fold, ads must not occupy in total more than 25 percent of the visible portion of the webpage.}}

[^1]: {{screen-desktop[footnote in "Other Acceptable Ads formats"] The 'common screen size' for desktop is 1366x768, based on data from <a href="http://gs.statcounter.com/#desktop-resolution-ww-monthly-201401-201412">StatCounter</a>.}}
[^2]: {{screen-mobile[footnote in "Other Acceptable Ads formats"] The 'common screen size' for mobile is 360x640, based on data from <a href="http://gs.statcounter.com/#mobile_resolution-ww-monthly-201401-201412">StatCounter</a>.}}
[^3]: {{screen-tablets[footnote in "Other Acceptable Ads formats"] The 'common screen size' for tablets is 768x1024, based on data from <a href="http://gs.statcounter.com/#tablet-resolution-ww-monthly-201401-201412">StatCounter</a>.}}
