### {{not-acceptable-ad[Section header] What is not considered an Acceptable Ad?}} {: #not-acceptable-ad }

{{unacceptable-ads[Text in "not Acceptable Ad" section] The following types of ads are currently unacceptable, and cannot be considered for inclusion on the whitelist:}} [^1]

* {{load-new-ads[Bullet point in "not Acceptable Ad" section] Ads that visibly load new ads if the Primary Content does not change}}
* {{hover-effect-ads[Bullet point in "not Acceptable Ad" section] Ads with excessive or non user-initiated hover effects}}
* {{animated-ads[Bullet point in "not Acceptable Ad" section] Animated ads}} [^2]
* {{autoplay-ads[Bullet point in "not Acceptable Ad" section] Autoplay-sound or video ads}}
* {{expanding-ads[Bullet point in "not Acceptable Ad" section] Expanding ads}}
* {{oversized-ads[Bullet point in "not Acceptable Ad" section] Generally oversized image ads}}
* {{interstitial-ads[Bullet point in "not Acceptable Ad" section] Interstitial page ads}}
* {{overlay-ads[Bullet point in "not Acceptable Ad" section] Overlay ads}}
* {{overlay-video-ads[Bullet point in "not Acceptable Ad" section] Overlay in-video ads}}
* {{pop-ups[Bullet point in "not Acceptable Ad" section] Pop-ups}}
* {{pop-unders[Bullet point in "not Acceptable Ad" section] Pop-unders}}
* {{pre-video-ads[Bullet point in "not Acceptable Ad" section] Pre-roll video ads}}
* {{rich-ads[Bullet point in "not Acceptable Ad" section] Rich media ads (e.g. Flash ads, Shockwave ads, etc.)}}

[^1]: {{exception[Footnote in "not Acceptable Ad" section] Except when the user intentionally interacts with the ad (e.g. clicks on the ad to see a video ad playing).}}
[^2]: {{exception-animated[Footnote in "not Acceptable Ad" section] With one exception, please see <a href="#mobile-acceptable-ads">06 Mobile Ads</a>}}
